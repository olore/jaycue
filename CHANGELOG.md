# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.4](https://github.com/olore/jaycue/compare/v1.2.3...v1.2.4) (2020-02-13)

### [1.2.3](https://github.com/olore/jaycue/compare/v1.2.0...v1.2.3) (2020-01-29)


### Bug Fixes

* **workflow:** actually do the push and publish ([39f836d](https://github.com/olore/jaycue/commit/39f836de06f0b660b9b730cf332d2e734d6dbe67))
* **workflow:** add username/token to push command ([1a8ce42](https://github.com/olore/jaycue/commit/1a8ce42ac7949907e8298abc1ad77e0913e3664d))
* **workflow:** hardcode repo url ([d41e3f2](https://github.com/olore/jaycue/commit/d41e3f21dbd2e8ef35e63d3c81e759b975b68d67))
* **workflow:** let's try with automatic GITHUB_TOKEN ([ddfb549](https://github.com/olore/jaycue/commit/ddfb549f8293dae11eb884bfd994bd86c1f43bc5))
* **workflow:** one more time for tonight ([23edcf8](https://github.com/olore/jaycue/commit/23edcf854796b3aa9c3bedacb1fa4aad06ffb8e0))
* **workflow:** oops, too many https ([32343fe](https://github.com/olore/jaycue/commit/32343fe2108a44f88df5cc6c3da4260b73401dea))
* **workflow:** push HEAD ([a5a2718](https://github.com/olore/jaycue/commit/a5a2718727bbf640fffa1f9ca7558b2721c0e786))
* **workflow:** push HEAD ([4bda9e7](https://github.com/olore/jaycue/commit/4bda9e751b679fb5e649f8f7f6f4a6992ed1818e))
* **workflow:** specific branch, not sure it's right ([8a51594](https://github.com/olore/jaycue/commit/8a51594163ee40e03bc0504515aaaffd458a132b))
* **workflow:** this time with user email/name ([4bdc550](https://github.com/olore/jaycue/commit/4bdc550e930a239276e9ef85622bb5323cc8083a))
* **workflow:** try HEAD:master ([8930831](https://github.com/olore/jaycue/commit/8930831fdbc88b283505af9fa1b4f568b22a26f6))
* **workflow:** try push to master ([80ce8b9](https://github.com/olore/jaycue/commit/80ce8b98ef8e51e76841f3b857c1764ecce39f09))
* learn how to do 'npm run' commands ([0353171](https://github.com/olore/jaycue/commit/0353171437e0f3f922c7de336105eb102b9580d2))
* set config to allow commit from action ([ba75f22](https://github.com/olore/jaycue/commit/ba75f2239a0bdceb67164149fc0732174c9c2d49))
